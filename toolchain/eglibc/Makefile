#
# Copyright (C) 2008 Luigi 'Comio' Mantellini <luigi.mantellini@idf-hit.com>
#                    Industrie Dial Face S.p.A.
# Copyright (C) 2006-2008 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk
include $(INCLUDE_DIR)/target.mk

PKG_NAME:=eglibc
PKG_EXTRAVERSION:=$(strip $(subst ",, $(CONFIG_EGLIBC_EXTRA_VERSION)))#"))

ifeq ($(CONFIG_EGLIBC_SVN),y)
    ifeq ($(strip $(subst ",, $(CONFIG_EGLIBC_VERSION_TRUNK))),y)#")))
	PKG_VERSION:=2.8
	PKG_SOURCE_VERSION:=$(CONFIG_EGLIBC_SVN_REVISION)
	PKG_SOURCE_URL:=svn://svn.eglibc.org/trunk
	PKG_SOURCE_SUBDIR:=eglibc
        PKG_SOURCE:=eglibc-trunk.tar_rev$(CONFIG_EGLIBC_SVN_REVISION).bz2
    else
	PKG_VERSION:=$(strip $(subst ",, $(CONFIG_EGLIBC_VERSION)))#"))
	PKG_SOURCE_VERSION:=$(CONFIG_EGLIBC_SVN_REVISION)
	PKG_SOURCE_URL:=svn://svn.eglibc.org/branches/$(PKG_NAME)-$(subst .,_,$(PKG_VERSION))
	PKG_SOURCE_SUBDIR:=$(PKG_NAME)-$(subst .,.,$(PKG_VERSION))
        PKG_SOURCE:=eglibc-$(PKG_VERSION)$(PKG_EXTRAVERSION)_rev$(CONFIG_EGLIBC_SVN_REVISION).tar.bz2
    endif
    PKG_SOURCE_VERSION:=$(strip $(subst ",, $(CONFIG_EGLIBC_SVN_REVISION)))#"))
else
    PKG_VERSION:=$(strip $(subst ",, $(CONFIG_EGLIBC_VERSION)))#"))
    PKG_SOURCE_URL:=http://svn.eglibc.org/branches/$(PKG_NAME)-$(subst .,_,$(PKG_VERSION))/
    PKG_SOURCE:=eglibc-$(PKG_VERSION)$(PKG_EXTRAVERSION).tar.bz2
endif

PATCH_DIR:=./patches/$(PKG_VERSION)$(PKG_EXTRAVERSION)
CONFIG_DIR:=./config/$(PKG_VERSION)$(PKG_EXTRAVERSION)

PKG_MD5SUM:=

# PKG_SOURCE_VERSION:=HEAD
# PKG_SOURCE_SUBDIR:=eglibc-$(PKG_VERSION)$(PKG_EXTRAVERSION)

PKG_CAT:=bzcat


ifeq ($(CONFIG_SOFT_FLOAT),y)
EGLIBC_FLOAT:=--with-fp=no
else
EGLIBC_FLOAT:=--with-fp=yes
endif

STAGING_DIR_HOST:=$(TOOLCHAIN_DIR)
BUILD_DIR_HOST:=$(BUILD_DIR_TOOLCHAIN)
PKG_BUILD_DIR:=$(BUILD_DIR_HOST)/eglibc-$(PKG_VERSION)$(PKG_EXTRAVERSION)

include $(INCLUDE_DIR)/host-build.mk

STAMP_BUILT:=$(TOOLCHAIN_DIR)/stamp/.eglibc_installed
STAMP_INSTALLED:=$(TOOLCHAIN_DIR)/stamp/.eglibc-utils_installed

EGLIBC_TARGET_ARCH:=$(shell echo $(ARCH) | sed -e s'/-.*//' \
		-e 's/i.86/x86/' \
		-e 's/sparc.*/sparc/' \
		-e 's/arm.*/arm/g' \
		-e 's/avr32.*/avr32/g' \
		-e 's/m68k.*/m68k/' \
		-e 's/ppc/powerpc/g' \
		-e 's/v850.*/v850/g' \
		-e 's/sh64/sh/' \
		-e 's/sh[234].*/sh/' \
		-e 's/m68k.*/m68k/' \
		-e 's/mips.*/mips/' \
		-e 's/mipsel.*/mips/' \
		-e 's/cris.*/cris/' \
)

GCC_SRC_DIR0:=$(BUILD_DIR_HOST)/gcc-$(strip $(subst ",, $(CONFIG_GCC_VERSION)))#"))
GCC_BUILD_DIR0:=$(BUILD_DIR_HOST)/gcc-$(strip $(subst ",, $(CONFIG_GCC_VERSION)))-bootstrap#"))
GCC_CROSS_DIR0:=$(BUILD_DIR_HOST)/eglibc_dev

define Build/Prepare
	mkdir -p $(BUILD_DIR_HOST)/eglibc-headers
	mkdir -p $(BUILD_DIR_HOST)/eglibc_dev/usr/include
	@echo prepare GLIBC version $(PKG_VERSION)$(PKG_EXTRAVERSION)
	$(call Build/Prepare/Default)
	@echo prepare gcc_stage0 in order to compile eglibc...
	ln -sf $(PKG_BUILD_DIR)/ports  $(PKG_BUILD_DIR)/libc/
	if [ ! -f $(BUILD_DIR_HOST)/eglibc_dev/usr/include/linux/version.h ] ; then \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm $(BUILD_DIR_HOST)/eglibc_dev/usr/include/; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-$(EGLIBC_TARGET_ARCH) $(BUILD_DIR_HOST)/eglibc_dev/usr/include/; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-generic $(BUILD_DIR_HOST)/eglibc_dev/usr/include/; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/linux $(BUILD_DIR_HOST)/eglibc_dev/usr/include/; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm $(TOOLCHAIN_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-$(EGLIBC_TARGET_ARCH) $(TOOLCHAIN_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-generic $(TOOLCHAIN_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/linux $(TOOLCHAIN_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm $(PKG_BUILD_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-$(EGLIBC_TARGET_ARCH) $(PKG_BUILD_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/asm-generic $(PKG_BUILD_DIR)/include/ ; \
		cp -pLR $(BUILD_DIR_TOOLCHAIN)/linux/include/linux $(PKG_BUILD_DIR)/include/ ; \
	fi;
	(cd $(BUILD_DIR_HOST)/eglibc-headers ; \
	 AUTOCONF="no" \
	 SHELL="$(BASH)" \
	 PATH=$(TARGET_PATH) \
	 BUILD_CC=$(HOSTCC) \
	 $(TARGET_CONFIGURE_OPTS) \
	 $(PKG_BUILD_DIR)/libc/configure \
	 	--prefix=/usr \
	 	--with-headers=$(BUILD_DIR_HOST)/eglibc_dev/usr/include \
	 	--build=$(GNU_HOST_NAME) \
	 	--host=$(REAL_GNU_TARGET_NAME) \
	 	--disable-profile \
	 	--without-gd \
	 	--without-cvs \
	 	--enable-add-ons \
		$(EGLIBC_FLOAT) \
	)
	(cd $(BUILD_DIR_HOST)/eglibc-headers ; \
	 SHELL="$(BASH)" \
	 PATH=$(TARGET_PATH) \
	 AUTOCONF="no" \
	 $(MAKE) AUTOCONF="no" install-headers install_root=$(BUILD_DIR_HOST)/eglibc_dev install-bootstrap-headers=yes \
	)
	mkdir -p $(BUILD_DIR_HOST)/eglibc_dev/usr/lib
	(cd $(BUILD_DIR_HOST)/eglibc-headers ; \
	 SHELL="$(BASH)" \
	 PATH=$(TARGET_PATH) \
	 BUILD_CC=$(HOSTCC) \
	 $(TARGET_CONFIGURE_OPTS) \
	 make csu/subdir_lib CFLAGS="$(TARGET_CFLAGS)" \
	)
	(cd $(BUILD_DIR_HOST)/eglibc-headers ; \
	 cp csu/crt1.o csu/crti.o csu/crtn.o $(BUILD_DIR_HOST)/eglibc_dev/usr/lib && \
	 PATH=$(TARGET_PATH) \
	 BUILD_CC=$(HOSTCC) \
	 $(TARGET_CONFIGURE_OPTS) \
	 $(TARGET_CC) $(TARGET_CFLAGS) -nostdlib -nostartfiles -shared -x c /dev/null \
	              -o $(BUILD_DIR_HOST)/eglibc_dev/usr/lib/libc.so \
	)
endef

define Build/Configure
endef

EGLIBC_MAKE := \
	PATH=$(TARGET_PATH) \
	AUTOCONF="no" \
	$(MAKE) $(TARGET_CONFIGURE_OPTS) AUTOCONF="no" \
		HOSTCC="$(HOSTCC)" \
		CPU_CFLAGS="$(TARGET_CFLAGS)"

define Build/Compile
	mkdir -p $(BUILD_DIR_HOST)/eglibc_build
	(cd $(BUILD_DIR_HOST)/eglibc_build ; \
	 AUTOCONF="no" \
	 $(PKG_BUILD_DIR)/libc/configure \
	    --prefix=/ \
	    --with-headers=$(BUILD_DIR_HOST)/eglibc_dev/usr/include \
	    --host=$(REAL_GNU_TARGET_NAME) \
	    --build=$(GNU_HOST_NAME) \
	    --disable-profile \
	    --without-gd \
	    --without-cvs \
	    --enable-add-ons \
	    $(EGLIBC_FLOAT) \
	)
	(cd $(BUILD_DIR_HOST)/eglibc_build ; \
	 $(EGLIBC_MAKE) \
	)
	(cd $(BUILD_DIR_HOST)/eglibc_build ; \
	 $(EGLIBC_MAKE) install install_root=$(TOOLCHAIN_DIR) ; \
	)
endef

define Build/Install
	mkdir -p $(TOOLCHAIN_DIR)/usr
	if [ ! -e $(TOOLCHAIN_DIR)/usr/include ]; then \
	    cd $(TOOLCHAIN_DIR)/usr ; \
	    ln -sf ../include include ; \
	fi;
endef

define Build/Clean
	rm -rf $(PKG_BUILD_DIR) $(BUILD_DIR_HOST)/eglibc_build $(BUILD_DIR_HOST)/eglibc_dev $(BUILD_DIR_HOST)/eglibc-headers
endef

$(eval $(call HostBuild))
